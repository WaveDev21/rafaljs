/**
 * Created by ituser on 2016-05-25.
 */

document.addEventListener("DOMContentLoaded", function() {


    //------------------------------ Klasa Procesora -------------------------------------------
    
    function Memory() {
        this.array = [];
        this.currentDisplayed = 0;
}
    
    function Processor(){

        var memory = new Memory();

        var previous = 0;
        var operation = '';


        //------------------------------ Funkcje obliczające -------------------------------------------

        var sumFunction = function(){
            this.number =  Number(display.innerHTML);

            switch (operation){
                case '+':
                    currOperation.innerHTML = currOperation.innerHTML + this.number + " = " + (previous + this.number);
                    break;
                case '-':
                    currOperation.innerHTML = currOperation.innerHTML + this.number + " = " + (previous - this.number);
                    break;
                case '*':
                    currOperation.innerHTML = currOperation.innerHTML + this.number + " = " + (previous * this.number);
                    break;
                case '/':
                    currOperation.innerHTML = currOperation.innerHTML + this.number + " = " + (previous / this.number);
                    break;
                case '%':
                    currOperation.innerHTML = currOperation.innerHTML + this.number + " = " + (previous % this.number);
                    break;
                case "^":
                    currOperation.innerHTML = currOperation.innerHTML + "<sup>" + this.number + "</sup> = " + Math.pow(previous,this.number);
                    break;
            }
        };

        var factorialFunction = function(){
            var liczba = Number(display.innerHTML);

            var suma = 1;

            while(liczba!=0){
                suma *= liczba;
                if(liczba<0){
                    liczba++;
                }else{
                    liczba--;
                }
            }

            currOperation.innerHTML = display.innerHTML +"! = " + suma;
        };

        //------------------------------ Dodawanie do pamięci ------------------------------------------------

        function addToMemory() {
            memory.array.push(currOperation.innerHTML);
            memory.array.slice(0, 10);
            memory.currentDisplayed = memory.array.length-1;
        }

        //------------------------------ Funkcje obsługujące zdarzenia -------------------------------------------

        this.numBeerFunction = function(){
            display.innerHTML = String(display.innerHTML) + String(this.getAttribute("value"));
        };

        this.oppEraFunction = function(){

            if ((this.getAttribute("value") == "-")&& (display.innerHTML == '')){
                display.innerHTML = "-";
            }else if((this.getAttribute("value") == "+")&& (display.innerHTML == '-')){
                display.innerHTML = "";
            }else if(display.innerHTML != "-"){
                previous = Number(display.innerHTML);
                display.innerHTML = "";
                operation = this.getAttribute("value");

                currOperation.innerHTML = String(previous) + operation;
            }
        };

        this.orDeerFunction = function(){

            switch (this.getAttribute("value")){
                case "=":
                    if(previous != 0){
                        sumFunction();
                        previous = 0;
                        display.innerHTML = "";

                        addToMemory();
                    }
                    break;
                case "!":
                    factorialFunction();
                    previous = 0;
                    display.innerHTML = "";
                    addToMemory();
                    break;
                case "<-":
                    display.innerHTML = display.innerHTML.slice(0, -1);
                    break;
                case "c":
                    currOperation.innerHTML = "";
                    previous = 0;
                    display.innerHTML = "";
                    break;
                case "<":
                        if(memory.currentDisplayed>0){
                            memory.currentDisplayed -= 1;
                            this.removeAttribute("disabled");
                        }else{
                            this.setAttribute("disabled", "");
                        }
                        currOperation.innerHTML = memory.array[memory.currentDisplayed];
                    break;
                case ">":
                    if(memory.currentDisplayed<(memory.array.length -1)){
                        memory.currentDisplayed += 1;
                        this.removeAttribute("disabled");
                    }else{
                        this.setAttribute("disabled", "");
                    }
                    currOperation.innerHTML = memory.array[memory.currentDisplayed];
                    break;
            }

        };

        //------------------------------ Funkcje ustawiające zdarzenia -------------------------------------------

        this.setNummericListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].addEventListener('click', this.numBeerFunction, false);
            }
        };

        this.setOpperationListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].addEventListener('click', this.oppEraFunction, false);
            }
        };

        this.setOrderListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].addEventListener('click', this.orDeerFunction, false);
            }
        };

        this.setOutputTargets = function(display, currOperation){
            this.display = display;
            this.currOperation = currOperation;
        };

        //------------------------------ Funkcje niszczące zdarzenia -------------------------------------------

        this.discardNummericListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].removeEventListener('click', this.numBeerFunction, false);
            }
        };

        this.discardOpperationListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].removeEventListener('click', this.oppEraFunction, false);
            }
        };

        this.discardOrderListeners = function(buttons){
            for (var i = 0; i < buttons.length; i++) {

                buttons[i].removeEventListener('click', this.orDeerFunction, false);
            }
        };

        //----------------------------- Czyszczenie wyświetlacza --------------------------------------------

        this.clearDisplay = function(){
            display.innerHTML = "";
            currOperation.innerHTML = "";
        };

    }

    // --------------------------------------- Klasa kalkulator -------------------------------------

    function Kalkulator(){

        var numericButtons = document.getElementsByClassName("number");
        var opperationButtons = document.getElementsByClassName("opperation");
        var orderButtons = document.getElementsByClassName("order");
        var powerButton = document.getElementById('power');
        var isOn = false;

        var processor = new Processor();

        this.display = document.getElementById("display");
        this.currOperation = document.getElementById('currOperation');

        this.power = function(){
            if(!isOn){

                processor.setNummericListeners(numericButtons);
                processor.setOpperationListeners(opperationButtons);
                processor.setOrderListeners(orderButtons);

                processor.setOutputTargets(this.display, this.currOperation);

                powerButton.innerHTML = "<h1>Off</h1>";
                isOn = true;
            }else{

                processor.discardNummericListeners(numericButtons);
                processor.discardOpperationListeners(opperationButtons);
                processor.discardOrderListeners(orderButtons);

                processor.clearDisplay();

                powerButton.innerHTML = "<h1>On</h1>";
                isOn = false;
            }
        };

        powerButton.addEventListener('click', this.power);


    }


    //------------------------------ Uwożenie Obiektu -------------------------------------------

    var kalkulatorObject = new Kalkulator();

});