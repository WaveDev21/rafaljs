/**
 * Created by Wave on 28.05.2016.
 */

document.addEventListener("DOMContentLoaded", function(){

    // ---------------------------------------- Twożymy klasy

    function Validator(){
        var str;

        this.vaidated = false;

        this.processRegExp = function(regExp){
            return regExp.test(str);
        };

        this.setStr = function(value){
            str = value;
        };

        this.deleteAllChilds = function(myNode){
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
        };
    };

    function NameValidator(){

        var regExp = /^[A-Z][a-z]{2,9}$/;

        this.validate = function(info, container){
            container.classList = "input-group";

            info.classList = "input-group-addon";

            this.glyphElement = document.createElement("span");
            this.additionalSpan = document.createElement("span");

            if(this.processRegExp(regExp)){
                this.deleteAllChilds(info);
                this.glyphElement.classList = "glyphicon glyphicon-ok-sign";
                info.appendChild(this.glyphElement);
                container.classList.add("correct");
                this.vaidated = true;
            }else{
                this.deleteAllChilds(info);
                this.glyphElement.classList = "chmurka";
                this.glyphElement.title = "Imie powinno zaczynać się z dużej litery i może mieć maksymalnie 10 liter długości";
                this.additionalSpan.classList = "glyphicon glyphicon-info-sign";
                this.additionalSpan.title = "";
                this.glyphElement.appendChild(this.additionalSpan);
                info.appendChild(this.glyphElement);
                container.classList.add("wrong");
                this.vaidated = false;
            }
        };
    };

    function SurnameValidator(){
        var regExp = /^[A-Z][a-z]{2,9}$/;

        this.validate = function(info, container){
            container.classList = "input-group";

            info.classList = "input-group-addon";

            this.glyphElement = document.createElement("span");
            this.additionalSpan = document.createElement("span");

            if(this.processRegExp(regExp)){
                this.deleteAllChilds(info);
                this.glyphElement.classList = "glyphicon glyphicon-ok-sign";
                info.appendChild(this.glyphElement);
                container.classList.add("correct");
                this.vaidated = true;
            }else{
                this.deleteAllChilds(info);
                this.glyphElement.classList = "chmurka";
                this.glyphElement.title = "Nazwisko powinno zaczynać się z dużej litery i może mieć maksymalnie 10 liter długości";
                this.additionalSpan.classList = "glyphicon glyphicon-info-sign";
                this.additionalSpan.title = "";
                this.glyphElement.appendChild(this.additionalSpan);
                info.appendChild(this.glyphElement);
                container.classList.add("wrong");
                this.vaidated = false;
            }
        };
    };

    function EmailValidator(){

        var regExp = /^[a-z]{3,13}@[a-z0-9]{2,6}\.[a-z]{2,4}$/;

        this.validate = function(info, container){
            container.classList = "input-group";

            info.classList = "input-group-addon";

            this.glyphElement = document.createElement("span");
            this.additionalSpan = document.createElement("span");

            if(this.processRegExp(regExp)){
                this.deleteAllChilds(info);
                this.glyphElement.classList = "glyphicon glyphicon-ok-sign";
                info.appendChild(this.glyphElement);
                container.classList.add("correct");
                this.vaidated = true;
            }else{
                this.deleteAllChilds(info);
                this.glyphElement.classList = "chmurka";
                this.glyphElement.title = "Email nie powinien zawierać . w nazwie max. długośc to 25 znaków";
                this.additionalSpan.classList = "glyphicon glyphicon-info-sign";
                this.additionalSpan.title = "";
                this.glyphElement.appendChild(this.additionalSpan);
                info.appendChild(this.glyphElement);
                container.classList.add("wrong");
                this.vaidated = false;
            }
        };
    };

    function PostalCodeValidator(){
        var regExp = /^[0-9]{2}-[0-9]{3}$/;

        this.validate = function(info, container){
            container.classList = "input-group";

            info.classList = "input-group-addon";

            this.glyphElement = document.createElement("span");
            this.additionalSpan = document.createElement("span");

            if(this.processRegExp(regExp)){
                this.deleteAllChilds(info);
                this.glyphElement.classList = "glyphicon glyphicon-ok-sign";
                info.appendChild(this.glyphElement);
                container.classList.add("correct");
                this.vaidated = true;
            }else{
                this.deleteAllChilds(info);
                this.glyphElement.classList = "chmurka";
                this.glyphElement.title = "Kod pocztowy składa się z 2 cyfr znaku - i 3 cyfr";
                this.additionalSpan.classList = "glyphicon glyphicon-info-sign";
                this.additionalSpan.title = "";
                this.glyphElement.appendChild(this.additionalSpan);
                info.appendChild(this.glyphElement);
                container.classList.add("wrong");
                this.vaidated = false;
            }
        };
    };

    function BirthDateValidator(){
        var regExp = /^[0-3][0-9]\.[01][0-9]\.[12][09][0-9]{2}$/;

        this.validate = function(info, container){
            container.classList = "input-group";

            info.classList = "input-group-addon";

            this.glyphElement = document.createElement("span");
            this.additionalSpan = document.createElement("span");

            if(this.processRegExp(regExp)){
                this.deleteAllChilds(info);
                this.glyphElement.classList = "glyphicon glyphicon-ok-sign";
                info.appendChild(this.glyphElement);
                container.classList.add("correct");
                this.vaidated = true;
            }else{
                this.deleteAllChilds(info);
                this.glyphElement.classList = "chmurka";
                this.glyphElement.title = "Format datu to dzień, miesiąc i rok przedzielone kropkami";
                this.additionalSpan.classList = "glyphicon glyphicon-info-sign";
                this.additionalSpan.title = "";
                this.glyphElement.appendChild(this.additionalSpan);
                info.appendChild(this.glyphElement);
                container.classList.add("wrong");
                this.vaidated = false;
            }
        };
    };

    // --------------------------------------- Ustawiamy dziedziczenia

    NameValidator.prototype = new Validator();
    SurnameValidator.prototype = new Validator();
    EmailValidator.prototype = new Validator();
    PostalCodeValidator.prototype = new Validator();
    BirthDateValidator.prototype = new Validator();

    // --------------------------------------- Twożymy obiekty


    var validators = {
        nameValidator : new NameValidator(),
        surnameValidator : new SurnameValidator(),
        emailValidator : new EmailValidator(),
        postalCodeValidator : new PostalCodeValidator(),
        birthDateValidator : new BirthDateValidator()
    };


    // --------------------------------------- Ustawiamy Listenery

    function form(form) {
        var currentForm = form;

        this.validatorListener = function () {
            switch (this.id) {
                case "name":
                    validators.nameValidator.setStr(this.value);

                    validators.nameValidator.validate(
                        document.getElementById(this.getAttribute("aria-describedby")),
                        this.parentNode
                    );
                    break;
                case "surname":
                    validators.surnameValidator.setStr(this.value);

                    validators.surnameValidator.validate(
                        document.getElementById(this.getAttribute("aria-describedby")),
                        this.parentNode
                    );
                    break;
                case "email":
                    validators.emailValidator.setStr(this.value);

                    validators.emailValidator.validate(
                        document.getElementById(this.getAttribute("aria-describedby")),
                        this.parentNode
                    );
                    break;
                case "postalCode":
                    validators.postalCodeValidator.setStr(this.value);

                    validators.postalCodeValidator.validate(
                        document.getElementById(this.getAttribute("aria-describedby")),
                        this.parentNode
                    );
                    break;
                case "birthDate":
                    validators.birthDateValidator.setStr(this.value);

                    validators.birthDateValidator.validate(
                        document.getElementById(this.getAttribute("aria-describedby")),
                        this.parentNode
                    );
                    break;
            }
        };

        this.submitListener = function(){

            var sum = true;

            for(var key in validators){
                sum &= validators[key].vaidated;
            }

            if (sum){
                currentForm.submit();
            }

        };


        this.setListeners = function () {
            var inputs = document.getElementsByClassName("validate");

            for (var i = 0; i < inputs.length; i++) {
                inputs[i].addEventListener("change", this.validatorListener, false);
            }

            document.getElementById("formSubmit").addEventListener("click", this.submitListener, false);
        };
    }



    var formObject  = new form(document.getElementById("form1"));
    formObject.setListeners();

/*
    document.getElementById("name").addEventListener("change", function(){
        alert("bla");
        nameValidator.setStr(this.value);
        nameValidator.validate(document.getElementById(this.getAttribute("aria-describedby")), this.parentNode);
    });

    document.getElementById("surname").addEventListener("change", function(){
        surnameValidator.setStr(this.value);
        surnameValidator.validate(document.getElementById(this.getAttribute("aria-describedby")), this.parentNode);
    });

    document.getElementById("email").addEventListener("change", function(){
        emailValidator.setStr(this.value);
        emailValidator.validate(document.getElementById(this.getAttribute("aria-describedby")), this.parentNode);
    });

    document.getElementById("postalCode").addEventListener("change", function(){
        postalCodeValidator.setStr(this.value);
        postalCodeValidator.validate(document.getElementById(this.getAttribute("aria-describedby")), this.parentNode);
    });

    document.getElementById("birthDate").addEventListener("change", function(){
        birthDateValidator.setStr(this.value);
        birthDateValidator.validate(document.getElementById(this.getAttribute("aria-describedby")), this.parentNode);
    });

*/
});