/**
 * Created by ituser on 2016-06-02.
 */

var app = angular.module('rankingApp',[]);
app.controller('countryControll',['$scope', '$http', '$filter', function ($scope, $http) {
    $http.get('json/uefa.json').success(function (data) {
       $scope.rank = data;
    })

    $scope.orderByThis = function (sort) {
        $scope.sortowanie = sort;
    };
    
    $scope.addItem = function (zm) {
        var obj = {
            "FIELD1":$scope.lp,
            "FIELD2":$scope.kraj,
            "FIELD8":$scope.punkty
        }

        $scope.rank.push(obj);

        $scope.lp = "";
        $scope.kraj = "";
        $scope.punkty = "";
    };

    $scope.removeItem = function (index) {
        $scope.rank.splice(index, 1);
    }
        
}]);

