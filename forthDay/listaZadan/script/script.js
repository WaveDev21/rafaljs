/**
 * Created by ituser on 2016-06-02.
 */

var app = angular.module('listaZadan',[]);

app.controller("listController", function($scope){
    $scope.lista = [];

    $scope.selected = {};

    $scope.getRemaining = function(){
        var odznaczone = 0;

        angular.forEach($scope.selected, function(value, key){
            if(value){
                odznaczone += 1;
            }
        });
        return odznaczone;
    };

    $scope.dodajZadanie = function(){
        $scope.lista.push($scope.newEx);
        $scope.newEx = "";
        console.log(JSON.stringify($scope.selected));
    };

    $scope.usunZakonczone = function(){
        angular.forEach($scope.selected, function(value, key){
            if(value){
                var index = $scope.lista.indexOf(key);
                $scope.lista.splice(index,1);
            }
        });
        $scope.selected = {};
    };
});


