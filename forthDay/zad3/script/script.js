/**
 * Created by ituser on 2016-06-02.
 */

var www = angular.module('stronaWWW',['ngRoute']);
    www.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
        when('/home', {
            templateUrl: 'home.html'
        }).
        when('/me', {
            templateUrl: 'omnie.html'
        }).
        when('/news', {
            templateUrl: 'newsy.html',
            controller: 'showNewsController'
        }).
        when('/news_more/:newsID', {
            templateUrl: 'news-more.html',
            controller: 'showOneNewController'
        }).
        when('/contact', {
            templateUrl: 'contact.html'
        }).
            otherwise({redirectTo: 'home'})
    }
    ]);

www.controller('showNewsController', function ($scope, $http) {
    $http.get('news.json').success(function (data) {
        $scope._news = data;
    });
});

www.controller('showOneNewController',function ($scope, $http, $routeParams) {
    $http.get('news.json').success(function (data) {
        $scope._title = data[$routeParams.newsID].title;
        $scope._desc = data[$routeParams.newsID].desc;
        $scope._img = data[$routeParams.newsID].img;
    });
});